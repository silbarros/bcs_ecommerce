**Features**<br>

eStore built with React and MongoDB
- This app can do CRUD operations and update the webpage products automatically after the database is changed
- The database is built online, using MondoDB Atlas
- The communication with the database is made via axios
- The user can register/login, add items to the cart and favorites. The total price is automatically calculated
- The passwords of the users are encrypted using argon2
- The user can pay the items and donate through Stripe
- It is possible to search for products
- The user can send messages to the admin. These messages are saved in the database
**Libraries used**

React.js 17.0.2

npm install --save react-router-dom

npm install axios

**How to Run It**

First clone the repo, then you will need 2 terminal windows.

In the first one you will run a client:

cd client

npm install

npm start


In the second one you will run a server:

cd server


npm install

nodemon  index.js


If you are using windows and receive a Python related error after running npm install from the server folder,run:

npm install --global --production windows-build-tools

