import React from "react";
import Checkout from "../pages/Checkout";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = props => {
  // console.log(process.env.REACT_APP_STRIPE_PUBLIC_KEY)
  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Checkout cartProds={props.cartProds} setCartProds={props.setCartProds} {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
