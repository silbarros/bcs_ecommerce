import React from "react";
import Donate from "../pages/Donate";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = props => {
  // console.log(process.env.REACT_APP_STRIPE_PUBLIC_KEY)
  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Donate {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
