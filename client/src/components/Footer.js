import React from 'react';
import {NavLink} from 'react-router-dom'
import '../App.css';

const Footer = (props) => {
  return(
    <div className='footer'>
    <footer>
        <div className="footer-wrapper">
          <div className="column1">
          <h2>Contacts</h2>
              <p><NavLink exact to={'/contact-us'}>Contact us</NavLink></p>
              <p>Address: A Street Full of Light, 4, 09865 , Blue Sky City</p>
              <p><a href="tel:+82123456790">+82 123 456 790</a></p>
              <p><a>Open hours: every day 10:00-20:00</a></p>
          </div>
          <div className="column2">
            <h2>Quick links</h2>
              <p><NavLink exact to={'/contact-us'}>Contact us</NavLink></p>
              <p><NavLink exact to={'/donate'}>Donate</NavLink></p>
          </div>
          <div className="column3">
            <h2>Donate</h2>        
              <p><NavLink exact to={'/donate'}>Help me developing apps!</NavLink></p>

          </div>
        </div>
    <p class="copyright">Copyright © 2021 — Fancie | Fancie Yourself </p>
  </footer>
</div>




  )
  };

export default Footer;
