import React from 'react';
import {NavLink} from 'react-router-dom'
import Search from './Search'
import '../App.css';

const Navbar=(props)=>{

    return (
    <div className='header'>
      <div className="navbar">
        <div className='navbar-left'>
          <NavLink exact to={'/product/shoes'} className='left-element'>Shoes</NavLink>
          <NavLink exact to={'/product/socks'} className='left-element'>Socks</NavLink>
          <NavLink exact to={'/product/t-shirts'} className='left-element'>T-shirts</NavLink>
          <NavLink exact to={'/product/bags'} className='left-element'>Bags</NavLink>
          <NavLink exact to={'/product/sales'} className='left-element'>Sales</NavLink>
        </div>
        <NavLink exact to={'/'} className='brandName'>FanciE</NavLink>

        <div className='navbar-right'>
          <Search {...props}/>
          {/* <div className="form-group fg--search">
            <input className='right-element search input' type='text' placeholder='What are you looking for?'/><button><i className="fa fa-search"></i></button>
          </div> */}
          <NavLink exact to={'/my-profile'} className='right-element'><i class="far fa-user-circle"></i></NavLink>
          <NavLink exact to={'/product/cart'} className='right-element'><i class="fas fa-shopping-cart"></i></NavLink>
          <NavLink exact to={'/product/my-favourites'} className='right-element'><i class="fas fa-heart"></i></NavLink>
        </div>
      </div>
    </div>
  );
  
}

export default Navbar;
