import React, {useEffect, useState} from 'react';
import axios from 'axios';
import { URL } from '../config';
import '../App.css';

const Search=(props)=>{
  const [prod,setProd]=useState('')
  const [results,setResults]=useState([])
  const [ok,setOK] = useState(null)

  const handleChange=(evt)=>{
    setProd(evt.target.value)
  }

  useEffect(()=>{
    async function fetchData(){
      const response = await axios.post(`${URL}/product/search`, {product:prod})
      setResults(response.data.prodExists);
      setOK(response.data.ok)
    }
    fetchData();
  },[prod])

  const handleSubmit=async (evt) => {
		evt.preventDefault();
        if (ok===true) {
				setTimeout(() => {
					props.history.push({
            pathname:'/search-results',
            state: {
              name: prod,
              products:results
            }
          });
				}, 500);
		  	}else{
				setTimeout(() => {
					props.history.push('/search-results-false');
				}, 500);		
        }    
    }

  return (
    <div className="form-group fg--search">
      <form className='search-form' onChange={handleChange} onSubmit={handleSubmit}>
        <input className='right-element search input' type='text' placeholder='What are you looking for?'/>
        <button><i className="fa fa-search"></i></button>
      </form>
    </div>
  );
  
}

export default Search;
