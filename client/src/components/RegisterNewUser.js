import '../App.css';
import React, {useState} from 'react'
import {NavLink} from 'react-router-dom'
import axios from 'axios';
import { URL } from '../config';

const RegisterNewUser=(props)=>{

	const [ form, setValues ] = useState({
		email: '',
		password: '',
		password2: ''
	});
  const [message,setMessage] = useState('')

  const handleChange = (evt) => {
		setValues({ ...form, [evt.target.name]: evt.target.value });
	};
  
	const handleSubmit = async(evt) => {
		evt.preventDefault();
		try {
			const response = await axios.post(`${URL}/users/register`, {
				email: form.email,
				password: form.password,
				password2: form.password2
			});
			setMessage(response.data.message);
			if (response.data.ok===true) {
				setTimeout(() => {
					props.history.push('/my-profile/login');
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};


  return (
    <div className="App">
      <h1>Register new user</h1>
      <form onSubmit={handleSubmit} onChange={handleChange}>
        Email:<input name='email'/><br></br>
        Password:<input name='password'/><br></br>
        Confirm Password:<input name='password2'/><br></br>
        <button>Register</button>
      </form>
      {message}
      <div className='profile-item'>
        <NavLink exact to={'/my-profile/login'}>Login</NavLink>
      </div>
    </div>
  );
}

export default RegisterNewUser;
