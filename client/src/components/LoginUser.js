import React, {useState} from 'react'
import {NavLink} from 'react-router-dom'
import axios from 'axios';
import '../App.css';
import { URL } from '../config';


const LoginUser=(props)=>{

  const [ form, setValues ] = useState({
		email: '',
		password: ''
	});
	const [ message, setMessage ] = useState('');

  const handleChange = (evt) => {
		setValues({ ...form, [evt.target.name]: evt.target.value });
	};

	const handleSubmit = async (evt) => {
		evt.preventDefault();
		try {
			const response = await axios.post(`${URL}/users/login`, {
				email: form.email,
				password: form.password
			});

			setMessage(response.data.message);

			if (response.data.ok) {
				setTimeout(() => {
					props.login(response.data.token);
					props.history.push('/secret-page');
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};
  

  return(
    <div className="App">
      <h1>Login</h1>
      <form onSubmit={handleSubmit} onChange={handleChange}>
       Email: <input name='email'/>
       Password: <input name='password'/>
       <button>Login</button>
      </form>    
      {message}
      <div className='profile-item'>
        <NavLink exact to={'/my-profile/register'}>Register new user</NavLink>
      </div>

     </div>
  );
}

export default LoginUser;

