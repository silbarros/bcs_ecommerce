import React,{useEffect, useState} from 'react';
import axios from 'axios';
import '../App.css';
import { URL } from '../config';
import Stripe from '../components/Stripe';

let type=null;

const Products=(props)=>{

  const [productsDB, setProductsDB] = useState([''])
  const [cartProds, setCartProds] = useState([])
  const [favProds, setFavProds] = useState([])

  type=props.match.params.type

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const favourites = JSON.parse(localStorage.getItem('favorites'));
    if(cart === null){
            localStorage.setItem('cart',JSON.stringify([]))
            setCartProds([])
            localStorage.setItem('favorites',JSON.stringify([]));
            return setFavProds([])
    }
    setCartProds(cart)
    setFavProds(favourites)
  },[])

  useEffect(()=>{
      let url=`${URL}/product/${type}`;
      axios.get(url)
          .then((res)=>{
            let data=res.data
            // console.log('reading from remote DB')
            setProductsDB(data)
          })
          .catch((error)=>{
            // console.log('Something went wrong. Not connected to DB', error)
          });        
  },[type]) //everytime type changes it's useEffect is called and the DB is fetched

  let products = []
  function addToCart(evt){
    let prods=[]
    
    cartProds.map((ele)=>{
      prods.push(ele.name)
    })
    if(cartProds.length===0){
      setCartProds([...cartProds, {name:evt.target.previousSibling.id, amount:evt.target.id, quantity:1, image:evt.target.nextSibling.id}])
    }
    if(!prods.includes(evt.target.previousSibling.id)){
      setCartProds([...cartProds, {name:evt.target.previousSibling.id, amount:evt.target.id, quantity:1, image:evt.target.nextSibling.id}])
    // console.log(cartProds)
    }
}

  useEffect(() => {
    localStorage.setItem('cart',JSON.stringify(cartProds));
  }, [cartProds]);

  // Add produts to favorites
  function addToFavs(evt){
    let prods=[]
    favProds.map((ele)=>{
      prods.push(ele.name)
    })
    if(favProds.length===0){
      setFavProds([...favProds, {name:evt.target.previousSibling.previousSibling.id, price:evt.target.previousSibling.id, image:evt.target.id}])
    }
    if(!prods.includes(evt.target.previousSibling.previousSibling.id)){
      setFavProds([...favProds, {name:evt.target.previousSibling.previousSibling.id, price:evt.target.previousSibling.id, image:evt.target.id}])
    }
  }
  useEffect(() => {
    localStorage.setItem('favorites',JSON.stringify(favProds));
  }, [favProds]);


  
//loop through all products of the type
  function loopProducts(arr) {
    for(var el of arr){
      if(el.category===type){
          products.push({image:el.image,name:el.name,price:el.price, onSale:el.onSale})
      }
    }

    return (
      <div className="products">
        <div className="products-title">
          {type==='my-favourites'?<h1>My Favourites</h1>:<h1>{type}</h1>}
        </div>
        <div className='products-wrapper'>
        {products.map((ele,idx)=>
          <div className='item' key={idx}>
            <p className='item-name' id={ele.image}>{ele.name}</p>
            <div className='image-background' id={ele.image}>
              <img src={ele.image} id={ele.image} value={ele.name} alt='product'/>
            </div>
            <p className='price' value={ele.price} id={ele.name}>{ele.price}€</p>
            <button className='item-button item-cart price' id={ele.price} onClick={addToCart}>Add to Cart</button>
            <button className='item-button item-favs' id={ele.image} value='favorites' onClick={addToFavs}>Add to favourites</button>
            {ele.onSale===true?<p className='sale'>On Sale!</p>:null}   
          </div>)}
        </div> 
      </div>   
    );
  }

  //loop through products on sale
  function onSale(arr){
    let prodsSale=[];
    for(var el of arr){
      if(type==='sales' && el.onSale===true){
        prodsSale.push({image:el.image,product:el.name,price:el.price, onSale:el.onSale, quantity:2, amount:10})
      }
  }

    return (
      <div className="sales-products">
        <div className='sales-products-wrapper'>
        {prodsSale.map((ele,idx)=>
          <div className='item' key={idx}>
            <img src={ele.image} alt='product in sale'/>
            <p className='name'>{ele.name}</p>
            <p className='price'>${ele.price}</p>
          </div>)}  
        </div> 
      </div>   
    );
  }
 
  //loop through all favourites
  function Favourites(arr){
    if(type==='my-favourites'){
    let inFavs=localStorage.getItem('favorites')
    let inFavsParsed=JSON.parse(inFavs)
    let emptyFavs=null;
    if(inFavsParsed===null){
      emptyFavs=true;
    }

    return(
      emptyFavs!==true&&inFavs.length>2?(
      <div className='favourites'>
        <div className='products-wrapper'>
          {JSON.parse(inFavs).map((ele,idx)=>(
            <div className='item cart-items'>
              <p className='item-name'>{ele.name}</p>
              <div className='image-background'>
                <img src={ele.image} alt='product'/>
              </div>
              {/* <p className='name' id={ele.name}>{ele.name}</p> */}
              <p className='price'>${ele.price}</p>
            </div>
        ))}
        </div>
       <button className="button-clear-all" onClick={function(){
          setFavProds([]);
          localStorage.removeItem('favorites');
       }}>Clear all favourites</button>
      </div>):<p>You have no favourites!</p>
    );}
  }

  return (
    <div>
    {loopProducts(productsDB)}
    {onSale(productsDB)}
    {type==='cart'?<Stripe cartProds={cartProds} setCartProds={setCartProds} {...props}/>:null}
    {Favourites(favProds)}
    </div>

  )
}

export default Products;
