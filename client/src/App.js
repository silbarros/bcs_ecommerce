import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import MyProfile from './pages/MyProfile';
import Contact from './pages/Contact';
import LoginUser from './components/LoginUser';
import RegisterNewUser from './components/RegisterNewUser';
import SecretPage from './pages/SecretPage'
import SearchResults from './pages/SearchResults'
import SearchResultsFalse from './pages/SearchResultsFalse'
import Stripe from "./components/Stripe";
import StripeDonate from "./components/StripeDonate";
import PaymentSuccess from "./pages/PaymentSuccess";
import PaymentError from "./pages/PaymentError";
import DonatePage from "./pages/DonatePage";
import Footer from './components/Footer'
import Products from './components/products'
import { URL } from './config';


function App(){
  const [ isLoggedIn, setIsLoggedIn ] = useState(false);

	const token = JSON.parse(localStorage.getItem('token'));

	const verify_token = async () => {
		if (token === null) return setIsLoggedIn(false);
		try {
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post(`${URL}/users/verify_token`);
			return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false);
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		verify_token();
	}, []);

	const login = (token) => {
		console.log('token ===>', token);
		localStorage.setItem('token', JSON.stringify(token));
		setIsLoggedIn(true);
	};
	const logout = () => {
		localStorage.removeItem('token');
		setIsLoggedIn(false);
	};


    return (
    <div className="App">
      <Router>
        {/* <Navbar className='navbar'/> */}
        <Route render={props => <Navbar {...props} />}/>
        <Route exact path='/' component={Home}/>
        <Route exact path='/product/:type' component={Products}/>
        <Route exact path='/my-profile' component={MyProfile}/>
        <Route exact path='/contact-us' component={Contact}/>
        <Route exact path='/donate' component={DonatePage}/>
        <Route exact path="/donate/stripe" render={props => <StripeDonate {...props} />} />

        <Route
				exact
				path="/my-profile/login"
				render={(props) => (isLoggedIn ? <Redirect to={'/secret-page'} /> : <LoginUser login={login} {...props} />)}
			/>
			<Route
				path="/my-profile/register"
				render={(props) => (isLoggedIn ? <Redirect to={'/secret-page'} /> : <RegisterNewUser {...props} />)}
			/>
			<Route
				path="/secret-page"
				render={(props) => (!isLoggedIn ? <Redirect to={'/'} /> : <SecretPage logout={logout} {...props} />)}
			/>

      <Route exact path="/payment/" render={props => <Stripe {...props} />} />

      <Route
        exact
        path="/payment/success"
        render={props => <PaymentSuccess {...props} />}
      />
      <Route
        exact
        path="/payment/error"
        render={props => <PaymentError {...props} />}
      />
      
			<Route
        exact
				path="/search-results"
        render={props => <SearchResults {...props} />}
			/>
			<Route
        exact
				path="/search-results-false"
        render={props => <SearchResultsFalse {...props} />}
			/>

      <Footer/>

      </Router>

      
    </div>
  );
  
}

export default App;
