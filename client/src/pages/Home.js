import React from 'react';
import {NavLink} from 'react-router-dom'
import '../App.css';
import homeImage from '../home_image.jpeg'
import wearingBag from '../wearing_bag.jpeg'
import wearingTShirt from '../wearing_tshirt.jpeg'
import wearingSocks from '../wearing_socks.jpeg'
import wearingShoes from '../wearing_shoes.jpeg'

const Home=()=>{

    return (
    <div className="home">
      <img className='home-image' src={homeImage} alt='people walking'></img>
      <div className='home-container'>
        <div>
            <img className="home-prod" alt="bag" src={wearingBag}></img>
            <p>Comfortable bags to carry your essentials.</p>
            <NavLink exact to={'/product/bags'} className='homeLink'><b>Show bags &gt;</b></NavLink>
        </div>

        <div>
            <img className="home-prod" alt="t-shirt" src={wearingTShirt}></img>
            <p>Summer is coming... it's time for light wear.</p>
            <NavLink exact to={'/product/t-shirts'} className='homeLink'><b>Show t-shirts &gt;</b></NavLink>
        </div>

        <div>
            <img className="home-prod" alt="socks" src={wearingSocks}></img>  
            <p>Comfy, funny socks. What else?</p>
            <NavLink exact to={'/product/socks'} className='homeLink'><b>Show socks &gt;</b></NavLink>
        </div>
      </div>

      <div className='home-sales-container'>
        <div className='home-sales-text'>
            <h2>Amazing Sales!</h2>
            <p>Don't miss some of your favourite products at a reduced price.</p>
            <p>This summer we are offering discounts on multiple items.</p>
            <NavLink exact to={'/product/sales'} className='homeLinkSales'><p><b><u>Check them out!</u></b></p></NavLink>
        </div>
        <div className='home-sales-section'>
            <NavLink exact to={'/product/sales'} className='homeLinkSales'><h2>Sales</h2></NavLink>
            <p className='homeLinkSalesEven'><b>Bags</b></p><br></br>
            <p className='homeLinkSalesEven'><b>T-shirts</b></p><br></br>
            <p className='homeLinkSalesEven'><b>Socks</b></p><br></br>
            <p className='homeLinkSalesEven'><b>Shoes</b></p><br></br>
        </div>
      </div>

      <div className='home-shoes-container'>
        <div className='home-shoes-column home-shoes-column1'>
            <img className="home-prod" alt="shoes" src={wearingShoes}></img>
            <p>Stylish yet comfy shoes that will take you anywhere.</p>
            <NavLink exact to={'/product/shoes'} className='homeLink'><b>Show shoes &gt;</b></NavLink>
        </div>
        <div className=' home-shoes-column home-shoes-column2'>
            <h2>Have you ever wondered...</h2><br></br>
            <p>... is it possible to have stylish yet casual, comfortable shoes?<br></br></p>
            <p>Well then... you will find them <NavLink exact to={'/product/shoes'} className='homeLinkShoes'><u>here</u></NavLink>!</p>
        </div>
      </div>
    </div>
  );
  
}

export default Home;
