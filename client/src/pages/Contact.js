import React, {useState} from "react";
import axios from 'axios';
import { URL } from '../config';
import '../App.css';

const Contact = () => {

	const [ form, setValues ] = useState({
		name: '',
		email: '',
		message: ''
	});
  const [message,setMessage] = useState('')


  const handleChange = (evt) => {
		setValues({ ...form, [evt.target.name]: evt.target.value });
	};

  const handleSubmit = async(evt) => {
		evt.preventDefault();
		try {
			const response = await axios.post(`${URL}/messages/add`, {
				name: form.name,
				email: form.email,
				message: form.message
			});
			setMessage(response.data.messageOutput);
      setValues({[evt.target.value]:''})
      setTimeout(() => {
        setMessage('')
      }, 1000);
      evt.target.reset()
		} catch (error) {
			console.log(error);
		}

	};

  return(
  <div className="contact-us">
    <form onChange={handleChange} onSubmit={handleSubmit}>
     <p>Name: <input placeholder='Your name' name='name'></input> </p>
     <p>E-email:<input placeholder='Your email' name='email'></input> </p>
      <p>Message:</p>
      <input placeholder='Your message' name='message' id='contact-message'></input> <br></br>
      <button className='button-send'>Send</button> <br></br>
      {message}
    </form>
    
  </div>
  )
};

export default Contact;