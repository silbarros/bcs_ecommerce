import React from 'react';
import axios from 'axios';
import { URL } from '../config';
import { injectStripe } from "react-stripe-elements";
import '../App.css';


const Donate = (props) => {
      //=== CREATE CHECKOUT SESSION ===
      const createDonateSession = async (amount) => {
        let inCartParsed=[{
          amount:amount,
          // images:["https://st3.depositphotos.com/4072575/15730/i/1600/depositphotos_157308020-stock-photo-macro-detailed-text-on-a.jpg"],
          name:'Donation',
          quantity:1}]
      try {
        const response = await axios.post(
          `${URL}/payment/create-checkout-session`,
          { inCartParsed }
        );
        console.log('response data===>',response.data)
        return response.data.ok
          ? (localStorage.setItem(
              "sessionId",
              JSON.stringify(response.data.sessionId)
            ),
            redirect(response.data.sessionId))
          : props.history.push("/payment/error");
      } catch (error) {
        console.log(error)
        props.history.push("/payment/error");
      }
    };

    //=== REDIRECT TO STRIPE CHECKOUT ===
    const redirect = sessionId => {
      props.stripe
        .redirectToCheckout({
          // Make the id field from the Checkout Session creation API response
          // available to this file, so you can provide it as parameter here
          // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
          sessionId: sessionId
        })
        .then(function(result) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
        });
    };

  return(
    <div className='donate'>
      <h2>Monetary Donation</h2>
        <div className="donate-wrapper">
          <button className="button" onClick={() => createDonateSession(5)}>5€</button>
          <button className="button" onClick={() => createDonateSession(10)}>10€</button>
          <button className="button" onClick={() => createDonateSession(15)}>15€</button>
          <button className="button" onClick={() => createDonateSession(20)}>20€</button>
          <button className="button" onClick={() => createDonateSession(25)}>25€</button>
          <button className="button" onClick={() => createDonateSession(50)}>50€</button>
        </div>
      <h2>Thank you for your support!</h2>

    </div>
  )
  };

// export default Donate;
export default injectStripe(Donate);