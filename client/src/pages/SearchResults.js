import React,{useState, useEffect} from 'react';
import { useLocation } from "react-router-dom";


const SearchResults = (props) => {
  const location = useLocation();
  const [productsResults, setProductsResults] = useState([]) 
  const [cartProds, setCartProds] = useState([])
  const [favProds, setFavProds] = useState([])


  useEffect(() => {
     setProductsResults(location.state.products);
  }, [location]);

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const favourites = JSON.parse(localStorage.getItem('favorites'));
    if(cart === null){
            localStorage.setItem('cart',JSON.stringify([]))
            setCartProds([])
            localStorage.setItem('favorites',JSON.stringify([]));
            return setFavProds([])
    }
    setCartProds(cart)
    setFavProds(favourites)
  },[])

  function addToCart(evt){
    let prods=[]
    cartProds.map((ele)=>{
      prods.push(ele.name)
    })

    if(cartProds.length===0){
      setCartProds([...cartProds, {name:evt.target.previousSibling.innerHTML, amount:evt.target.id, quantity:1, image:evt.target.previousSibling.id}])
    }
    if(!prods.includes(evt.target.previousSibling.innerHTML)){
      setCartProds([...cartProds, {name:evt.target.previousSibling.innerHTML, amount:evt.target.id, quantity:1, image:evt.target.previousSibling.id}])
    }
}

  useEffect(() => {
    localStorage.setItem('cart',JSON.stringify(cartProds));
  }, [cartProds]);

  // Add produts to favorites
  function addToFavs(evt){
    let prods=[]
    favProds.map((ele)=>{
      prods.push(ele.name)
    })
    if(favProds.length===0){
      setFavProds([...favProds, {image:evt.target.previousSibling.previousSibling.id, name:evt.target.previousSibling.previousSibling.innerHTML, price:evt.target.previousSibling.id, image:evt.target.previousSibling.previousSibling.id}])
    }
    if(!prods.includes(evt.target.previousSibling.previousSibling.innerHTML)){
      setFavProds([...favProds, {image:evt.target.previousSibling.previousSibling.id, name:evt.target.previousSibling.previousSibling.innerHTML, price:evt.target.previousSibling.id, image:evt.target.previousSibling.previousSibling.id}])
    }
  }
  useEffect(() => {
    localStorage.setItem('favorites',JSON.stringify(favProds));
  }, [favProds]);

  function loopProducts(arr) {
    return (
      <div className="products">
        <div className='products-wrapper'>
        {arr.map((ele,idx)=>
          <div className='item' key={idx}>
            <img src={ele.image} value={ele.image} alt='product'/>
            <p className='name' id={ele.image}>{ele.name}</p>
            <button id={ele.price} value={ele.image} onClick={addToCart}>Add to Cart</button>
            <button value='favorites' onClick={addToFavs}>Add to favourites</button>
            <p className='price' value={ele.price}>${ele.price}</p>
            {ele.onSale===true?<p className='sale'>On Sale!</p>:null}   
          </div>)}
        </div> 
      </div>   
    );
  }

	return (
		<div className="search_results">
			<h1>Results for: {props.location.state.name}</h1>
      {loopProducts(productsResults)}
		</div>
	);
};

export default SearchResults;
