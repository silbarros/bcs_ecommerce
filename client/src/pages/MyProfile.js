import React from 'react'
import {NavLink} from 'react-router-dom'
import '../App.css';

const MyProfile=()=>{

  return (
    <div className='myProfile'>
      <NavLink exact to={'/my-profile/login'}>Login</NavLink>
      <NavLink exact to={'/my-profile/register'}>Register new user</NavLink>
    </div>
    );
}

export default MyProfile;
