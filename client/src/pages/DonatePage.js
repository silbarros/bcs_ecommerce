import React from 'react';
import '../App.css';
import StripeDonate from '../components/StripeDonate';

const DonatePage=(props)=>{
  return (
    <div>
      <StripeDonate {...props}/>
    </div>

  )
}

export default DonatePage;
