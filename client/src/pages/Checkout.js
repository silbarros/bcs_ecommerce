import React from 'react';
import axios from 'axios';
import { URL } from '../config';
import { injectStripe } from "react-stripe-elements";


const Checkout = props =>{
    let inCart=localStorage.getItem('cart')
    let inCartParsed=JSON.parse(inCart)
    let newProducts = [...props.cartProds];


    let emptyCart=null;
    if(inCartParsed===null){
      emptyCart=true;
    }
    
    //=== CALCULATE TOTAL FUNCTION ===
    const calculate_total = () => {
      let total = 0;
      props.cartProds.forEach(ele => (total += ele.quantity * ele.amount));
      return total;
    };
    
    //=== CREATE CHECKOUT SESSION ===
    const createCheckoutSession = async () => {
        inCartParsed.forEach( ele => {
        ele.images = [ele.image];
        delete ele.image
      })
      try {
        const response = await axios.post(
          `${URL}/payment/create-checkout-session`,
          { inCartParsed }
        );
        return response.data.ok
          ? (localStorage.setItem(
              "sessionId",
              JSON.stringify(response.data.sessionId)
            ),
            redirect(response.data.sessionId))
          : props.history.push("/payment/error");
      } catch (error) {
        props.history.push("/payment/error");
      }
    };

    //=== REDIRECT TO STRIPE CHECKOUT ===
    const redirect = sessionId => {
      props.stripe
        .redirectToCheckout({
          // Make the id field from the Checkout Session creation API response
          // available to this file, so you can provide it as parameter here
          // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
          sessionId: sessionId
        })
        .then(function(result) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
        });
    };

    //Handle change of quantity
    const handleClick= (evt)=>{
      newProducts.map((ele,idx)=>{
        if(evt.target.parentNode.id===ele.name){
          if(evt.target.value==='less' && ele.quantity>0){
            ele.quantity-=1;
            return props.setCartProds(newProducts)
          }else if(evt.target.value==='more'){
            ele.quantity+=1;
            return props.setCartProds(newProducts)
          }else if(evt.target.value==='remove'){
            newProducts.splice(idx,1)
            return props.setCartProds(newProducts)
          }
        }
      })
      localStorage.setItem('cart',JSON.stringify(newProducts))
    }
    
    const handleClear=(evt)=>{
      localStorage.removeItem('cart')
      localStorage.setItem('cart',JSON.stringify([]))
      props.setCartProds([])
    }
    
    return(
      emptyCart!==true&&inCart.length>2?(
      <div className="cart">
        <div className='products-wrapper'>
        {props.cartProds.map((ele,idx)=>(
          <div className='item cart-items' key={idx} id={ele.name}>
            <p className='item-name' id={ele.name}>{ele.name}</p>
            <div className='image-background'>
              <img src={ele.image} alt='product'/>
            </div>
            <p className='price'>{ele.amount}€</p>
            <div id={ele.name} className='buttons-qtt'>
              <button className='button-qtt button-less' value='less' onClick={handleClick}>-</button> {ele.quantity} <button className='button-qtt button-more' value='more' onClick={handleClick}>+</button>
            </div>
            <button className='item-button button-remove' value='remove' onClick={handleClick}>Remove</button>          
          </div>
        ))}
        </div>
        <div className='total-cart'>
          <div className="total">Total : {calculate_total()} €</div>
          <button className="button-pay" onClick={() => createCheckoutSession()}>Pay Now</button><br></br>
          <button className="button-clear-all" value='clearAll' onClick={handleClear}>Clear all items</button>
         </div>
      </div>):<p>The cart is empty!</p>
    );
  
}

export default injectStripe(Checkout);

