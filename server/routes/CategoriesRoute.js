const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/CategoriesController');

//  == This route will give us back all todos: ==  //

router.get('/', controller.findAll);

// == Route to add category
router.post('/add', controller.addCategory);

// == Route to delete category
router.post('/delete', controller.deleteCategory);

// == Route to update category
router.post('/update', controller.updateCategory);

// // == Route to show all category
router.get('/categories', controller.allCategory);

// == Route to show specific category
// router.post('/category', controller.oneCategory);



module.exports = router;