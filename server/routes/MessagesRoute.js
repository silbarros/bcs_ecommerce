const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/MessagesController');

//ADMIN
// see all messages
router.get('/', controller.findAll);

// // == Route to add Message 
router.post('/add', controller.addMessage);

// // == Route to delete Message 
router.post('/delete', controller.deleteMessage);





module.exports = router;