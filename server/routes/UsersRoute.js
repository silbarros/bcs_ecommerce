const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/UsersController');

//  == This route will give us back all todos: ==  //

router.post('/register', controller.register);
router.post('/login', controller.login);
router.post('/verify_token', controller.verify_token);


//ADMIN
// see all users
router.get('/', controller.findAll);

// == Route to add User to category
router.post('/add', controller.addUser);

// // == Route to delete User to category
router.post('/delete', controller.deleteUser);





module.exports = router;