const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/ProductsController');

//  == This route will give us back all todos: ==  //

router.get('/', controller.findAll);

//  == This route will give us back one todo, it will be that with the id we are providing: ==  //
// router.get('/:product_id', controller.findOne);

// == Route to add product to category
router.post('/add', controller.addProduct);

// == Route to delete product to category
router.post('/delete', controller.deleteProduct);

// == Route to update product to category
router.post('/update', controller.updateProduct);

// == Route to show specific products
router.post('/product', controller.oneProduct);

// == Route to show products of category
router.get('/:type', controller.catProducts);

// == Route to search products
router.post('/search', controller.searchProduct);


module.exports = router;