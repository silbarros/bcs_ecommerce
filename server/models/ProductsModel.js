const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productsSchema = new Schema({
        category: String,
        // _id:Number,
        name:String,
        price: Number,
        color:String,
        description:String,
        image: String,
        onSale: String,
})
module.exports =  mongoose.model('products', productsSchema);
