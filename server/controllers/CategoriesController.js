const Categories = require('../models/CategoriesModel');

class CategoriesController {
    // GET FIND ALL
    async findAll(req, res){
      try{
          const categories = await Categories.find({});
          res.send(categories);
      }
      catch(e){
          res.send({e})
      }
  }

  // ADD CATEGORY
  async addCategory(req,res){
    let {category_name:nameToAdd} = req.body;
    let exists= await Categories.exists({category_name:nameToAdd})
    try{
      exists?
      res.send(`The category with name=${nameToAdd} already exists`):(
      Categories.create({name:nameToAdd,categories:[]}),
      res.send(`Category with name=${nameToAdd} was added to the database`
    ))
    }
    catch(e){
      res.send({e})
    }
  }

  //Delete Category
  async deleteCategory(req,res){
    let {category_ID:toDel} = req.body;
    let exists= await Categories.exists({_id:toDel})
    try{
     exists?(await Categories.deleteOne({_id:toDel}),res.send(`Category ${toDel} was deleted from the database`)):res.send(`The category ${toDel} does not exist`)
    }
    catch(error){
      res.send({error})
    }
  }

  //Update Category
  async updateCategory(req,res){
    let {category_ID:idToUpdate, category_newName:newName} = req.body;
    let exists= await Categories.exists({_id:idToUpdate})
    try{
      exists?(await Categories.updateOne({_id:idToUpdate},{ name:newName }), res.send(`The name of category with ID ${idToUpdate} was updated to ${newName}`)):
      res.send(`The category with ID ${idToUpdate}  does not exist in the database so it cannot be updated`)
    }
    catch(error){
      res.send({error})
    }
  }

  //Show All Categories nicely
    async allCategory(req,res){
      let allCategories=[]
      try{
        const categories = await Categories.find({});
        categories.map((ele,idx)=>{allCategories.push(ele.name)})
        res.send(`All categories:${allCategories.join(', ')}`)
      }
      catch(error){
        res.send({error})
      }
    }


};
module.exports = new CategoriesController();