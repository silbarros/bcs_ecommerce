const Users = require('../models/UsersModel');
const argon2 = require('argon2'); //https://github.com/ranisalt/node-argon2/wiki/Options
const validator = require('validator');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const jwt_secret = process.env.JWT_SECRET;


class UsersController {
  // GET FIND ALL
  async findAll(req, res){
    try{
        const users = await Users.find({});
        res.send(users);
    }
    catch(e){
        res.send({e})
    }
  }

  // Add user to DB
  async addUser(req, res){
    let {user_email:userEmail, user_password:userPass}=req.body
    let exists= await Users.exists({email:userEmail})
    try{
      if(exists){
        res.send(`The email ${userEmail} is already registered`)
      }else{
        Users.create({email:userEmail,password:userPass})
        res.send(`The user was created with the email ${userEmail}`)
      }
    }
    catch(e){
      res.send(e)
    }
  }
      
  async deleteUser(req,res){
    let {user_email:userEmail, user_password:userPass}=req.body
    let exists= await Users.exists({email:userEmail})
    try{
      if(exists){
        await Users.deleteOne({ email: userEmail  });
        res.send(`The user ${userEmail} was successfully deleted`)
      }else{
        res.send(`The user ${userEmail} does not exist in the DB and hence cannot be deleted`)
      }
    }
    catch(error){
      res.send({error})
    }
  }

  async register(req,res){
// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password,
//     password2: form.password2
//  }
    const { email, password, password2 } = req.body;
    if (!email || !password || !password2) return res.json({ ok: false, message: 'All fields are required' });
    if(password.length<8) return res.json({ok: false, message:'the password needs to be at least 8 characters long'})
    if (password !== password2) return res.json({ ok: false, message: 'passwords must match' });
    if (!validator.isEmail(email)) return res.json({ ok: false, message: 'please provide a valid email' });
    try {
      const user = await Users.findOne({ email });
      if (user) return res.json({ ok: false, message: 'email already in use' });
      const hash = await argon2.hash(password);
      console.log('hash ==>', hash);
      const newUser = {
        email,
        password: hash
      };
      await Users.create(newUser);
      res.json({ ok: true, message: 'successfully registered' });
    } catch (error) {
      res.json({ ok: false, error });
    }
  };

  async login(req,res){
    // the client is sending this body object
//  {
//     email: form.email,
//     password: form.password
//  }
    const{email, password}=req.body;
    if (!email || !password) return res.json({ ok: false, message: 'All field are required' });
    if (!validator.isEmail(email)) return res.json({ ok: false, message: 'invalid email format' });
    try {
      const user = await Users.findOne({ email });
      if (!user) return res.json({ ok: false, message: 'user does not exist' });
      const match = await argon2.verify(user.password, password);
      if (match) {
        const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: '1h' }); //{expiresIn:'365d'} // i can have jwt.sign(user.toJSON()) only, not include passwords (the whole user information) to the token
        res.json({ ok: true, message: 'welcome back', token, email });
      } else return res.json({ ok: false, message: 'password incorrect' });
    } catch (error) {
      res.json({ ok: false, error });
    }
  };
  
  async verify_token(req,res){
    console.log(req.headers.authorization);
    const token = req.headers.authorization;
    jwt.verify(token, jwt_secret, (err, succ) => {
      err ? res.json({ ok: false, message: 'something went wrong' }) : res.json({ ok: true, succ });
    });
  };

};

module.exports = new UsersController();