const Products = require('../models/ProductsModel');
const Categories = require('../models/CategoriesModel');

class ProductsController {
    // GET FIND ALL
  async findAll(req, res){
    try{
        const products = await Products.find({});
        res.send(products);
    }
    catch(e){
        res.send({e})
    }
  }

  // Add product to category
  async addProduct(req, res){
    let {category:prodCategory,product_name:prodToAdd, product_price:prodPrice, product_color:prodColor, product_description:prodDesc,product_image:prodImg,onSale:prodOnSale}=req.body
    const categoryExists = await Categories.exists({name:prodCategory});
    try{
      if(!categoryExists){
        res.send(`The category ${prodCategory} does not exist`)
      }else{
        const prodExists = await Products.exists({product_name:prodToAdd});
        if(prodExists){
          res.send(`The product with ID=${prodToAdd} already exists!`)
        }else{
          Products.create({category:prodCategory, name:prodToAdd,price:prodPrice,color:prodColor,description:prodDesc, image:prodImg, onSale:prodOnSale})
          console.log(await Products.find())
          res.send(`The product ${prodToAdd} was added with category ${prodCategory}`)
        }
      }
    }
    catch(e){
      res.send(e)
    }
  }

  //delete product
  async deleteProduct(req, res){
    let {product_id:prodToRemove}=req.body
    let exists= await Products.exists({_id:prodToRemove})
    try{
      if(exists){
        await Products.deleteOne({_id:prodToRemove});
        res.send(`The user ${prodToRemove} was successfully deleted`)
      }else{
        res.send(`The product ${prodToRemove} does not exist in the DB and hence cannot be removed`)
      }
    }
    catch(error){
      res.send({error})
    }
  }

  //update product information
  async updateProduct(req,res){
    let {product_ID:prodID, product_NewName:prodNewName, product_NewPrice:prodNewPrice, product_NewColor:prodNewColor, product_NewDescription:prodNewDesc, product_NewImage:prodNewImg, product_NewOnSale:prodNewOnSale}=req.body
    try{
      prodNewName!=''?(await Products.findOneAndUpdate({_id:prodID},{name: prodNewName})):null
      prodNewPrice!=''?(await Products.findOneAndUpdate({_id:prodID},{price: prodNewPrice})):null
      prodNewColor!=''?(await Products.findOneAndUpdate({_id:prodID},{color: prodNewColor})):null
      prodNewDesc!=''?(await Products.findOneAndUpdate({_id:prodID},{description: prodNewDesc})):null
      prodNewImg!=''?(await Products.findOneAndUpdate({_id:prodID},{image: prodNewImg})):null
      prodNewOnSale!=''?(await Products.findOneAndUpdate({_id:prodID},{onSale: prodNewOnSale})):null

      res.send(`The product ${prodID} was successfully updated`)
    }
    catch(e){
      res.send(e)
    }     
  }

  //Show Specific Product
  async oneProduct(req,res){
    let {product_id:prodID}=req.body
    let exists= await Products.exists({_id:prodID})
    try{
      if(!exists){
        res.send(`The product with ID=${prodID} does not exist`)
      }else{
        res.send(await Products.find({_id:prodID}))
      }
    }
    catch(e){
      res.send(e)
    }
  }

  async catProducts(req,res){
    let cat = req.params.type
    try{
      const catProds = await Products.find({category:cat});
      res.send(catProds);
  }
  catch(e){
      res.send({e})
  }
  }

  //Product search by the user
  async searchProduct(req,res){
    // the client is sending this object
    //  product:prod
    const {product} = req.body;
    const regex = new RegExp(product, 'i') // i for case insensitive
    try {
      const prodExists = await Products.find({name: {$regex: regex}})
      if(prodExists.length!=0){
        res.json({ ok: true, message: 'product exists', prodExists})
      }else{
        res.json({ ok: false, message: 'product does not exist' })
      }
    }catch (error) {
      res.json({ ok: false, error });
    }
  }

};
module.exports = new ProductsController();
