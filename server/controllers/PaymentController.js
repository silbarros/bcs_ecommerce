const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const create_checkout_session = async (req, res) => {
  try {
    const domainURL = process.env.DOMAIN;
    const { inCartParsed } = req.body;
    if (inCartParsed.length < 1 || !inCartParsed)
      return res.send({
        ok: false,
        message: "Please select at least 1 product"
      });
    inCartParsed.forEach(item => {
      item.currency = process.env.CURRENCY;
      item.amount *= 100;
    });
    // Create new Checkout Session for the order
    // Other optional params include:
    // [billing_address_collection] - to display billing address details on the page
    // [customer] - if you have an existing Stripe Customer ID
    // [payment_intent_data] - lets capture the payment later
    // [customer_email] - lets you prefill the email input in the form
    // For full details see https://stripe.com/docs/api/checkout/sessions/create
    session = await stripe.checkout.sessions.create({
      payment_method_types: process.env.PAYMENT_METHODS.split(", "),
      line_items: inCartParsed,
      // ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID set as a query param
      success_url: `${domainURL}/payment/success?session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `${domainURL}/`
    });
    return res.send({ ok: true, sessionId: session.id });
  } catch (error) {
    console.log("ERROR =====>", error.raw.message);
    return res.send({ ok: false, message: error.raw.message });
  }
};

const checkout_session = async (req, res) => {
  try {
    const { sessionId } = req.query;
    const session = await stripe.checkout.sessions.retrieve(sessionId);
    const customer = await stripe.customers.retrieve(session.customer);
    return res.send({ ok: true, session: session, customer });
  } catch (error) {
    console.log("ERROR =====>", error.raw.message);
    return res.send({ ok: false, message: error.raw.message });
  }
};

module.exports = {
  create_checkout_session,
  checkout_session
};
