const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    categoriesRoute = require('./routes/CategoriesRoute'),
    productsRoute = require('./routes/ProductsRoute'),
    usersRoute = require('./routes/UsersRoute'),
    messagesRoute = require('./routes/MessagesRoute'),
    paymentRoute = require('./routes/PaymentRoute'),
    bodyParser = require('body-parser');
    require('dotenv').config();
// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo
const cors = require('cors');
const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb+srv://silviabarros:15084287@ecommerceapp.jns2e.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/category', categoriesRoute);
app.use('/product', productsRoute);
app.use('/users', usersRoute);
app.use('/messages', messagesRoute);
app.use('/payment',paymentRoute);
// Set the server to listen on port 3001
app.listen(3001, () => console.log(`listening on port 3001`))